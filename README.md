# Tag Albums

## Description
Tag Albums has several features. When first installed, it will provide a sidebar link labeled "View Tag Albums". This link will allow visitors to the Gallery to browse through a list of all the tags. This list resembles separate sub-albums, one for each tag. Cover art for each "virtual album" is taken from the most recently uploaded photo/album/video for each tag. The photo pages that are accessed from within these "virtual albums" have been modified so that the previous / next buttons navigate within the virtual album instead of the album the photo is actually in, and "fake" breadcrumbs are created to further the illusion that the user is in a real album.

Additionally, the module allows for real albums to be linked to the "View Tag Albums" page, via the edit album dialog so that when a user clicks into a specific album, they will be directed to the tag albums page instead of the albums contents.

In addition to linking an album to the "View Tag Albums" page, this module also allows for albums to be linked to specific tag sub-albums or to virtual albums generated from combinations of tags. These virtual pages maintain breadcrumbs and previous / next buttons that maintain the appearance of being in the original album and copy the title, description, sort order from the album that linked to the page.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "tag_albums" folder from the zip file into your Gallery 3 modules folder.  Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  Once activated you will be able to position the sidebar from the Admin -> Appearance -> Manage Sidebar screen and configure the sort order of the Tag Albums page from the Settings -> Tag Albums Settings page.

To link an album to a dynamic page, follow these steps:
 - Create a new album.
 - (Optional) Upload a photo into the album to act as it's thumbnail (if you skip this step, the album won't have a thumbnail).
 - Select Album Options - Edit Album.  You'll see a drop down box that says "Display items that contain ANY of the following tags" / "Display items that contain ALL of the following tags", select one of these options and then list on or more tags in the box directly below it.  If this second box contains just a "*" instead of a tag, the album will re-direct to the "View Tag Albums" screen.

## History
**Version 4.0.0:**
> - Completely reworked code to comply with Gallery's new virtual album support (found in the 3.0.3 release).
> - Removed all theme overrides and the "Theme Files" folder -- These are no longer necessary.
> - Released 27 June 2012.
>
> Download: [Version 4.0.0](/uploads/fdb38b84a7eaa8db035e806d0fe8e957/tag_albums400.zip)

**Version 3.0.0:**
> - Added option to allow admin's to set thumbnails for tag albums using any of the photos in the virtual album.
> - Released 02 April 2012.
>
> Download: [Version 3.0.0](/uploads/c39a7bb33605075ef7d02c2d61f42cdb/tag_albums300.zip)

**Version 2.3.0:**
> - Compatibility fix for the Custom Fields module.
> - Released 09 November 2011.
>
> Download: [Version 2.3.0](/uploads/276b0f59b50353cf6f8808135ab47abf/tag_albums230.zip)

**Version 2.2.0:**
> - Updated GreyDragon theme support for version 3.1.0 release.
> - Released 12 October 2011.
>
> Download: [Version 2.2.0](/uploads/4ce33bde159fbf8b9e79b728f6497de2/tag_albums220.zip)

**Version 2.1.0:**
> - Updated for Gallery 3.0.2 compatibility.
> - Updated theme support files for current releases.
> - Module now appends the file name to the end of the url for consistency with the rest of Gallery.
> - Released 14 September 2011.
>
> Download: [Version 2.1.0](/uploads/47e7871936cca83e8ed9422d7eb3034b/tag_albums210.zip)

**Version 2.0.0:**
> - Added ability to filter the contents of the "All Tags" album page by the first letter of the tag.
> - Added an option for replacing the "All Tags" album page with either the "All Tags" module or the "Tag Cloud Page" module.
> - Updated Breadcrumbs code so that clicking on a photos album will load the page the photo is in instead of page 1.
> - Improved Info Module integration -- mousing over a thumbnail will now display its view counts and owner name.
> - Bug Fix:  Viewing a photo/video through a virtual album will now increase it's view count.
> - Bug Fix:  Display "Title" instead of "File Name" on thumbnails.
> - Theme Support: Now compatible with Gallery 3.0.1's Wind theme and the Grey Dragon 3.0.6 theme.
> - Released 11 May 2011.
>
> Download: [Version 2.0.0](/uploads/a09c8b6c835be739d9025fec78de1204/tag_albums200.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 05 May 2011
>
> Download: [Version 1.0.0](/uploads/f4b75c1bfd20e53b3b4e2aff7ae4cef6/tag_albums100.zip)
